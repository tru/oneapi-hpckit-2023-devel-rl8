FROM rockylinux:8
#FROM registry-gitlab.pasteur.fr/tru/docker-rl9-ci:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

# https://www.intel.com/content/www/us/en/docs/oneapi/installation-guide-linux/2023-0/yum-dnf-zypper.html
ADD oneAPI.repo /etc/yum.repos.d/oneAPI.repo
RUN dnf -y install intel-hpckit

